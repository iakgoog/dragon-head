import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpInterceptorService } from 'src/shared/services/httpInterceptorService';
import { HTTP_INTERCEPTORS, HttpClientModule,HttpClient } from '@angular/common/http';
import { ExampleDataService } from 'src/shared/dataService/exampleDataservice';
import { MapViewComponent } from './map-view/map-view.component';
import { HighchartsChartModule } from 'highcharts-angular';
import {  } from 'selenium-webdriver/http';
import { AuthService } from 'src/shared/services/authService';
export function init_app(authService: AuthService) {
  return () => authService.getAccessToken();
}

@NgModule({
  declarations: [
    AppComponent,
    MapViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HighchartsChartModule,
    HttpClientModule,
  ],
  providers: [
    // HttpClient,
    ExampleDataService,
    AuthService,
  { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
  { provide: APP_INITIALIZER, useFactory: init_app, deps: [AuthService], multi: true },
  
],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
