import { Component } from '@angular/core';
import { Router } from '@angular/router';

const DEFAULT_TITLE = 'Hello!';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = DEFAULT_TITLE;
  username = null;
  password = null;

  private timeout;
  constructor(private router:Router){}

  get valid () {
    return !this.username || !this.password;
  }

  login () {
    this.title = 'Done!';
    console.log("mappp")
    this.router.navigate(['/maps']);
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.title = DEFAULT_TITLE;
      this.username = null;
      this.password = null;
    }, 2000);
  }

}
