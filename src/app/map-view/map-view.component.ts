declare var require: any;

import { Component } from '@angular/core';
import * as Highcharts from 'highcharts';
import MapModule from 'highcharts/modules/map';
import ExportingModule from 'highcharts/modules/exporting';
import SunsetTheme from 'highcharts/themes/sunset';
const mapWorld = require('@highcharts/map-collection/custom/world.geo.json');
const mapAsia = require('@highcharts/map-collection/custom/asia.geo.json');
import * as HC_customEvents from 'highcharts-custom-events';
import { ExampleDataService } from 'src/shared/dataService/exampleDataservice';
HC_customEvents(Highcharts);
MapModule(Highcharts);
ExportingModule(Highcharts);
SunsetTheme(Highcharts);

Highcharts.setOptions({
  title: {
    style: {
      color: 'tomato'
    }
  },
  legend: {
    enabled: false
  }
});

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.less']
})
export class MapViewComponent {
  Highcharts: typeof Highcharts = Highcharts; // Highcharts, it's Highcharts
  displayData:any = []
  listData:any[];

  chartMap: Highcharts.Options = {
    chart: {
      map: mapAsia
    },
    title: {
      text: 'Highmaps basic demo'
    },
    subtitle: {
      text: 'Source map: <a href="http://code.highcharts.com/mapdata/custom/world.js">World, Miller projection, medium resolution</a>'
    },
    mapNavigation: {
      enabled: true,
      buttonOptions: {
        alignTo: 'spacingBox'
      }
    },
    legend: {
      enabled: true
    },
    colorAxis: {
      min: 0
    },
    plotOptions: {
      series: {
          events: {
              click:function(e){
                console.log("plot event",e);
              }
          }
        }
      },
    series: [{
      name: 'Random data', 
      states: {
        hover: {
          color: '#BADA55'
        }
      },
      dataLabels: {
        enabled: true,
        format: '{point.name}'
      },
      // events:{
      //   click: function(events){
      //     console.log(events)
      //   }
      // },
      allAreas: true,
      data:
      [
        ['ir', 555],
        ['tl', 81],
        ['th', 122],
        ['kg', 211],
        ['np', 212]
      ]
    } as Highcharts.SeriesMapOptions,
    ]
  }

  constructor(private exampleDataService:ExampleDataService){
    this.listData = [
      { label: 'Asia', type: 'header' },
      { label: 'China', selected: 'true' },
      { label: 'Japan' },
      { label: 'Korea' },
      { type: 'divider' },
      { label: 'Europe', type: 'header' },
      { label: 'Spain' },
      { label: 'France', disabled: 'true' }
    ];
    console.log("list data ",this.listData);
    console.log("country in asia",this.exampleDataService.getCountryFromJSON("asia"))
    
  }

}


