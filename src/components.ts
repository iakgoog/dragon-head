// Polyfills
import '@elf/polyfills';

// Components
import '@elf/coral-button';
import '@elf/coral-input';
import '@elf/coral-panel';

// Themes (Imported last)
import '@elf/elf-theme-elemental/light/coral-button';
import '@elf/elf-theme-elemental/light/coral-input';
import '@elf/elf-theme-elemental/light/coral-panel';