import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

const abd = require('../../assets/country-by-abbreviation.json');
const continent = require('../../assets/country-by-continent.json');
const cc = require('../../assets/country-by-currency-code.json');
const countryData = require('../../assets/countryData.json');


@Injectable()
export class ExampleDataService {
    
    constructor(public http:HttpClient) {

    }

    public getAllClass(){
        return new Promise<any>((resolve,reject)=>{
            this.http.get("https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/us-population-density.json")
            .subscribe((data: any) => { 
                resolve(data)
            })
        })
        
    }
    public getCountryFromJSON(continent?:any){
        if(continent){
            return countryData.filter(obj=>{
                if(obj.continent !== null){
                    return obj.continent.toLowerCase() === continent.toLowerCase();
                }
            })
        }else{
            return countryData;
        }
    }
    
            
}