import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClient, HttpHeaders
} from '@angular/common/http';
import { AuthService } from './authService';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
    private authToken:any;
    constructor(private authService:AuthService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        // Get the auth token from the service.
        

        /*
        * The verbose way:
        // Clone the request and replace the original headers with
        // cloned headers, updated with the authorization.
        const authReq = req.clone({
        headers: req.headers.set('Authorization', authToken)
        });
        */
        // Clone the request and set the new header in one step.
        // const authReq = req.clone();
        let authReq
        if(this.authService.getCurrentToken() !== "Bearer undefined"){
          authReq = req.clone({
            headers: req.headers.set('Authorization', this.authService.getCurrentToken())
          });
        }else{
          authReq = req.clone();
        }
        

        // send cloned request with header to the next handler.
        return next.handle(authReq);
    }



    
}