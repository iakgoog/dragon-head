import { Injectable, ErrorHandler } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";



@Injectable()
export class AuthService {
    private currentToken:string
    private currentRefreshToken:string
    
    constructor(public http:HttpClient) {

    }

    getAccessToken(){
      let url = "/auth/oauth2/beta1/token"
      let username = "kittipong_p.edpdev@refinitiv.com"
      let password = "Mininovpad55"
      let body = `username=${username}&password=${password}&grant_type=password&scope=trapi&takeExclusiveSignOnControl=true`
      setInterval(()=>{
        this.getRefreshToken();
      },2*60*1000)

      this.http.post(url,body,{
            headers: {'Content-Type':'application/x-www-form-urlencoded','Authorization':'Basic MDYyNmMwM2Q0NzJmNDA4YTg2ODY1MmJkMDYyODViZWU3ZDczNDBkZTo='}
        }).subscribe(
          (message:any)=>{
            console.log("get token success",message)
            this.currentToken = message.access_token;
            this.currentRefreshToken = message.refresh_token;
          }
          ,err=>{
            console.log("get token error",err)
          })
    }

    getRefreshToken(){
      let url = "/auth/oauth2/beta1/token"
      let username = "kittipong_p.edpdev@refinitiv.com"
      let body = `username=${username}&grant_type=password&refresh_token=${this.currentRefreshToken}`

      this.http.post(url,body,{
          headers: {'Content-Type':'application/x-www-form-urlencoded','Authorization':'Basic MDYyNmMwM2Q0NzJmNDA4YTg2ODY1MmJkMDYyODViZWU3ZDczNDBkZTo='}
      }).subscribe(
        (message:any)=>{
          console.log("get refresh token success",message);
          this.currentToken = message.access_token;
        }
        ,err=>{
          console.log("get token error",err)
        })
    }

    getCurrentToken(){
      return "Bearer "+ this.currentToken
    }
    
            
}